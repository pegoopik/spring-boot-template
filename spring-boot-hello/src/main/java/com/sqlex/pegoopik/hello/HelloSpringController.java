package com.sqlex.pegoopik.hello;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class HelloSpringController {

    /**
     * ping
     * @return pong
     */
    @LoadBalanced
    @ApiOperation("Ping Test")
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/ping")
    public String ping() {
        log.info("ping");
        return "pong";
    }

}
