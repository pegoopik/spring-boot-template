package com.sqlex.pegoopik.hello;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class HelloSpringBootTest {

    @Test
    public void test() {
        assertEquals("Hello " + "JUnit", "Hello JUnit");
    }

}
