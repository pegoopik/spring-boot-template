package com.sqlex.pegoopik.hello;

import org.junit.Rule;
import org.junit.Test;
import org.testcontainers.containers.PostgreSQLContainer;

import static org.junit.Assert.assertNotNull;

public class TestContainerExampleTest {

    @Rule
    public PostgreSQLContainer postgres = new PostgreSQLContainer();

    @Test
    public void test() {
        assertNotNull(postgres.getJdbcUrl());
    }
}

