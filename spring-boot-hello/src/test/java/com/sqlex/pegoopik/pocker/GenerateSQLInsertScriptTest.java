package com.sqlex.pegoopik.pocker;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.*;
import java.net.URL;
import java.util.Scanner;
//import java.util.stream.Collectors;

public class GenerateSQLInsertScriptTest {

    private static final String FILE_NAME = "input.txt";
    private static int count = 0;

    @Test
    public void test() throws FileNotFoundException {
        URL resource = this.getClass().getClassLoader().getResource(FILE_NAME);
        Scanner in = new Scanner(new FileReader(resource.getFile()));
        System.out.println("SELECT * FROM (VALUES");
        while(in.hasNext()) {
            String sqlLine = getSqlLine(in.nextLine());
            sqlLine += in.hasNext() ? "," : "";
            System.out.println(sqlLine);
        }
        System.out.println(")T(" + getFieldList() + ")");
    }

    private String getSqlLine(String line) {
        String sqlLine = "(";
        int i=0;
        for (String value : StringUtils.split(line, "\t")) {
            sqlLine += i++ > 0 ? "," : "";
            sqlLine += StringUtils.isEmpty(value) ? "null" : "'" + value + "'";
        }
        count = count > i ? count : i;
        sqlLine += ")";
        return sqlLine;
    }

    private String getFieldList() {
        String fieldList = "";
        for (int i=0; i<count; i++) {
            fieldList += i == 0 ? "" : ",";
            fieldList += "t"+(i+1);
        }
        return fieldList;
    }

}
